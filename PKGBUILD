# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Based on the file created for Arch Linux by: Frank Vanderham

_linuxprefix=linux-lqx
_extramodules=extramodules-6.2-lqx
pkgname=${_linuxprefix}-broadcom-wl
_pkgname=broadcom-wl
_pkgver=6.30.223.271
pkgver=6.30.223.271_6.2.10.lqx1_1
pkgrel=1
pkgdesc='Broadcom 802.11 Linux STA wireless driver BCM43142.'
url='https://bbs.archlinux.org/viewtopic.php?id=145884'
arch=('x86_64')
license=('custom')
makedepends=("broadcom-wl-dkms>=${_pkgver}"
             'dkms'
             "${_linuxprefix}" "${_linuxprefix}-headers")
groups=("${_linuxprefix}-extramodules")
provides=("${_pkgname}=${_pkgver}")
install=${_pkgname}.install
backup=('etc/modprobe.d/${_linuxprefix}-broadcom-wl.conf')
source=(broadcom-wl-dkms.conf)
sha256sums=('b97bc588420d1542f73279e71975ccb5d81d75e534e7b5717e01d6e6adf6a283')

pkgver() {
    _ver=$(pacman -Q ${_linuxprefix} | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  # build host modules
  msg2 'Build module'

  CFLAGS="${CFLAGS} -Wno-error"
  CXXFLAGS="${CXXFLAGS}  -Wno-error"

  msg2 "$CFLAGS, ${CXXFLAGS}"

  fakeroot dkms build --dkmstree "${srcdir}" -m "broadcom-wl/${_pkgver}" -k "${_kernver}"
}

package(){
  _ver=$(pacman -Q ${_linuxprefix} | cut -d " " -f 2)
  depends=("${_linuxprefix}=${_ver}")

  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
  install -dm755 "${pkgdir}/usr/lib/modules/${_extramodules}"
  cd "broadcom-wl/${_pkgver}/${_kernver}/$CARCH/module"
  install -m644 * "${pkgdir}/usr/lib/modules/${_extramodules}"
  find "${pkgdir}" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/${_pkgname}.install"
  install -D -m 644 "${srcdir}"/broadcom-wl-dkms.conf "${pkgdir}/etc/modprobe.d/${_linuxprefix}-broadcom-wl.conf"
}
